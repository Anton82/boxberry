<?php
namespace Tests\Service\Api;

use AppBundle\Services\Delivery\Api\BoxBerry;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Created by PhpStorm.
 * User: writer
 * Date: 14.09.2016
 * Time: 17:23
 */
class BoxBerryTest extends WebTestCase
{
    public function testGetCityListMethod()
    {
        $api = new BoxBerry();
        $r   = $api->getCityList();
        $this->assertNotEmpty($r);
        $this->assertArrayHasKey('Code', $r[0]);
        $this->assertArrayHasKey('Name', $r[0]);

    }

    public function testGetCityPointsMethod()
    {
        $api = new BoxBerry();
        $r   = $api->getPointsListByCityID(68);
        $this->assertNotEmpty($r);
        $this->assertArrayHasKey('Code', $r[0]);
        $this->assertArrayHasKey('Name', $r[0]);
    }

    public function testGetDeliveryPriceMethod()
    {
        $api = new BoxBerry();
        $r   = $api->getDeliveryPrice(77441, 5, 5000);
        $this->assertNotNull($r);
        $this->assertArrayHasKey('price', $r);
    }
}