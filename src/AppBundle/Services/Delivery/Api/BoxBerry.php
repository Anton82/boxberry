<?php
namespace AppBundle\Services\Delivery\Api;
/**
 * Created by PhpStorm.
 * User: writer
 * Date: 14.09.2016
 * Time: 17:15
 */
class BoxBerry
{
    private $token = '11570.pbpqebfc';

    public function getCityList()
    {
        $url = sprintf('http://api.boxberry.de/json.php?token=%s&method=ListCities', $this->token);

        return $this->request($url);
    }

    private function request($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);

        return json_decode($response, true);
    }

    public function getPointsListByCityID($cityID)
    {
        $url = sprintf('http://api.boxberry.de/json.php?token=%s&method=ListPoints&CityCode=%d', $this->token, $cityID);

        $points = $this->request($url);
        if (isset($points[0]['err'])) {
            return [];
        }

        return $points;
    }

    public function getDeliveryPrice($pointID, $weight, $orderSum)
    {
        $url = sprintf(
            'http://api.boxberry.de/json.php?token=%s&method=DeliveryCosts&weight=%d&target=%d&ordersum=%d&deliverysum=0&paysum=%d',
            $this->token, $weight, $pointID, $orderSum, $orderSum);

        $res = $this->request($url);
        if (isset($data[0]['err'])) {
            return null;
        }

        return $res;
    }
}