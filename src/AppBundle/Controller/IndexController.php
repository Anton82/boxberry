<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class IndexController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $api    = $this->get('api_boxberry');
        $cities = $api->getCityList();
        usort($cities, function ($a, $b) {
            if ($a['Name'] == $b['Name']) {
                return 0;
            }

            return ($a['Name'] < $b['Name']) ? -1 : 1;
        });

        return $this->render('AppBundle:Index:index.html.twig', [
            'cities' => $cities
        ]);
    }

    /**
     * @Route("calc",name="calculate")
     */
    public function CalculateAction(Request $request)
    {
        $api      = $this->get('api_boxberry');
        $pointID  = (int)$request->query->get('point-id');
        $weight   = (int)$request->query->get('order-weight');
        $orderSum = $request->query->get('order-sum');
        if ($pointID>0 && $weight > 0) {
            $price = $api->getDeliveryPrice($pointID, $weight, $orderSum);

            if ($price) {
                return new JsonResponse(['price'  => $price['price'],
                                         'period' => $price['delivery_period'],
                                         'status' => 1
                ]);
            }
        }

        return new JsonResponse(['status' => 0]);
    }

    /**
     * @Route("/getPoints/{code}", name="get-points", requirements={"code":"\d+"})
     * @param $code
     * @return JsonResponse
     */
    public function GetPoints($code)
    {
        $api    = $this->get('api_boxberry');
        $points = $api->getPointsListByCityID($code);
        if (!empty($points)) {
            $html = $this->renderView('@App/Index/points.html.twig', ['points' => $points]);

            return new JsonResponse(['html' => $html, 'status' => 1]);
        }

        return new JsonResponse(['status' => 0]);

    }
}
